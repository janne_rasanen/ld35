﻿using UnityEngine;
using System.Collections;


public class CloudGenerator : MonoBehaviour {
    public GameObject cloudPrefab;

    public int startingCloudAmount;
    public float cloudChancePerSec = 6f / 60f;
    public Vector2 posLimitX;
    public Vector2 posLimitY;
    public float cloudPartMaxShift = 1f;
    public Vector2 velocityLimit;

    void Start () {
        for(int i=0; i < transform.childCount; i++)
        {
            Destroy(transform.GetChild(i).gameObject);
        }

        for (int i = 0; i < startingCloudAmount; i++)
            GenerateCloud();
	}

    private void GenerateCloud()
    {
        Vector3 pos = transform.position;

        pos.x += Random.Range(posLimitX.x, posLimitX.y);
        pos.y += Random.Range(posLimitY.x, posLimitY.y);

        GameObject go = (GameObject)Instantiate(cloudPrefab, pos, Quaternion.identity);
        go.transform.SetParent(transform);

        for(int i=0; i < go.transform.childCount; i++)
        {
            Transform t = go.transform.GetChild(i);
            t.localPosition = Random.insideUnitSphere * cloudPartMaxShift;
        }

        go.GetComponent<Cloud>().velocity = Random.Range(velocityLimit.x, velocityLimit.y);
    }

    /*
    void Update () {
        if (Random.Range(0, 1) < cloudChancePerSec * Time.deltaTime)
            GenerateCloud();
	}
    */


}
