﻿using UnityEngine;
using System.Collections;

public class Node : MonoBehaviour {

    public GameObject cube;
    public GameObject indicator;

	void Start () {
	    
	}
	
	public void ActivateCube()
    {
        cube.GetComponent<Rigidbody>().useGravity = true;
        cube.GetComponent<BoxCollider>().enabled = true;
        cube.GetComponent<MeshRenderer>().enabled = true;

        indicator.GetComponent<MeshRenderer>().enabled = false;
    }

    public void ActivateIndicator()
    {
        cube.GetComponent<Rigidbody>().useGravity = false;
        cube.GetComponent<BoxCollider>().enabled = false;
        cube.GetComponent<MeshRenderer>().enabled = false;

        indicator.GetComponent<MeshRenderer>().enabled = true;
    }
}
