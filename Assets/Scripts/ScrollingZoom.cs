﻿using UnityEngine;
using System.Collections;

public class ScrollingZoom : MonoBehaviour {
    public Vector2 limits;
    public float velocity = 1f;

    private TrackingCamera tracking;
	void Start () {
        tracking = GetComponent<TrackingCamera>();
	}
	
	void Update () {
        float diff = Input.mouseScrollDelta.y * velocity;
        tracking.offset.z = Mathf.Clamp(tracking.offset.z + diff, limits.x, limits.y);
	}
}
