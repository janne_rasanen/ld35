﻿using UnityEngine;
using System.Collections;

public class AnimationDelay : MonoBehaviour {
    public float delayAmountSecs;

    private Animator animator;
	void Start () {
        animator = GetComponent<Animator>();
        animator.enabled = false;
        Invoke("ActivateAnimator", delayAmountSecs);
    }
	
	void ActivateAnimator()
    {
        animator.enabled = true;
    }
}
