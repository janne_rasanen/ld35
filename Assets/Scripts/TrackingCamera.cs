﻿using UnityEngine;
using System.Collections;

public class TrackingCamera : MonoBehaviour {
    public Grid grid;
    public Vector3 offset;
    public float maxVelocityPerSec = 1f;
    public float maxRotationPerSec = 1f;

	
	void Update () {
        Vector3 point = grid.GetCenterPoint();
        transform.position = Vector3.Lerp(transform.position, point + offset, Time.deltaTime * maxVelocityPerSec);

        transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(point - transform.position), Time.deltaTime * maxRotationPerSec);
	}
}
