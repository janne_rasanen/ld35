﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;

public class Grid : MonoBehaviour {
    public GameObject cubePrefab;
    public GameObject indicatorPrefab;
    public Transform startPos;
    public int cubeAmountLimit;
    private int cubeAmount = 0;
    public AudioSource cubeAppear;
    public AudioSource cubeDisappear;

    private class Node
    {
        public GameObject cube;
        public GameObject[] indicators = new GameObject[4];
    }

    private float betweenNodes = 1.2f;
    private List<Node> nodes = new List<Node>();

	void Start () {
        Invoke("CreateFirstNode", 0.5f);
	}
	
	private void CreateFirstNode()
    {
        CreateCube(startPos.position, Quaternion.identity, Vector3.zero, Vector3.zero);
    }

    private Node CreateCube(Vector3 pos, Quaternion rot, Vector3 velocity, Vector3 angularVelocity)
    {
        GameObject cube = CreateAt(cubePrefab, pos, rot, velocity, angularVelocity);
        Node node = new Node { cube = cube };
        nodes.Add(node);
        CreateIndicators(cube, node, velocity, angularVelocity);
        RemoveIndicatorsWithTooManyNeighbours();
        string debug = node.cube.name + " -> ";
        for (int i = 0; i < node.indicators.Length; i++)
            if (node.indicators[i])
                debug += node.indicators[i].name + " ";
        Debug.Log(debug);
        cubeAmount++;
        return node;
    }

    private GameObject CreateAt(GameObject prefab, Vector3 pos, Quaternion rot, Vector3 velocity, Vector3 angularVelocity)
    {
        GameObject go = (GameObject)Instantiate(prefab, pos, rot);
        Rigidbody r = go.GetComponent<Rigidbody>();
        r.velocity = velocity;
        r.angularVelocity = angularVelocity;
        go.name = prefab.name + " " + go.GetInstanceID();
        go.transform.SetParent(transform, true);
        return go;
    }

    private void CreateIndicators(GameObject cube, Node node, Vector3 velocity, Vector3 angularVelocity)
    {
        Transform t = cube.transform;

        Vector3[] pos = GetIndicatorWorldPositions(t);

        for (int i = 0; i < node.indicators.Length; i++)
        {
            if (!IndicatorExistsAt(pos[i], t.rotation) && !CubeExistsAt(pos[i], t.rotation))
                node.indicators[i] = CreateAt(indicatorPrefab, pos[i], t.rotation, velocity, angularVelocity);
        }

        Rigidbody rootRB = cube.GetComponent<Rigidbody>();
        for(int i=0; i < node.indicators.Length;i++)
        {
            if (node.indicators[i])
                node.indicators[i].AddComponent<FixedJoint>().connectedBody = rootRB;
        }
    }

    private Vector3[] GetIndicatorWorldPositions(Transform t)
    {
        return new Vector3[]
        {
            t.position + t.TransformVector(new Vector3(betweenNodes, 0, 0)),
            t.position + t.TransformVector(new Vector3(-betweenNodes, 0, 0)),
            t.position + t.TransformVector(new Vector3(0, betweenNodes, 0)),
            t.position + t.TransformVector(new Vector3(0, -betweenNodes, 0))
        };
    }

    private bool IndicatorExistsAt(Vector3 pos, Quaternion rotation)
    {
        Vector3 halfExtents = new Vector3(0.5f, 0.5f, 0.5f);
        Collider[] results = Physics.OverlapBox(pos, halfExtents, rotation);
        for(int i=0; i < results.Length; i++)
        {
            if (results[i].GetComponent<Indicator>())
            {
                return true;
            }
        }
        return false;
    }

    private bool CubeExistsAt(Vector3 pos, Quaternion rotation)
    {
        Vector3 halfExtents = new Vector3(0.5f, 0.5f, 0.5f);
        Collider[] results = Physics.OverlapBox(pos, halfExtents, rotation);
        for (int i = 0; i < results.Length; i++)
        {
            if (results[i].GetComponent<Cube>())
            {
                return true;
            }
        }
        return false;
    }

    public void SwapToCube(GameObject indicator)
    {
        if (cubeAmount > cubeAmountLimit)
            return;
        Transform t = indicator.transform;
        Rigidbody r = indicator.GetComponent<Rigidbody>();
        Node node = CreateCube(t.position, t.rotation, r.velocity, r.angularVelocity);

        FixedJoint joint = indicator.GetComponent<FixedJoint>();
        node.cube.AddComponent<FixedJoint>().connectedBody = joint.connectedBody;
        joint.connectedBody.gameObject.AddComponent<FixedJoint>().connectedBody = node.cube.GetComponent<Rigidbody>();

        Destroy(indicator);
        cubeAppear.Play();
    }

    public void SwapToIndicator(GameObject cube)
    {
        int id = cube.GetInstanceID();
        int index = nodes.FindIndex(n => n.cube.GetInstanceID() == id);
        Node node = nodes.ElementAt(index);
        nodes.RemoveAt(index);

        for (int i = 0; i < node.indicators.Length; i++)
        {
            if (node.indicators[i])
                Destroy(node.indicators[i]);
        }

        FixedJoint joint = cube.GetComponents<FixedJoint>().First(j => j.connectedBody != null);

        // fetch the node
        int otherCubeId = joint.connectedBody.gameObject.GetInstanceID();
        Node otherCubeNode = nodes.First(n => n.cube.GetInstanceID() == otherCubeId);

        // create indicator with a fixedjoint connection
        Transform t = cube.transform;
        Rigidbody r = cube.GetComponent<Rigidbody>();
        GameObject indicator = CreateAt(indicatorPrefab, t.position, t.rotation, r.velocity, r.angularVelocity);
        indicator.AddComponent<FixedJoint>().connectedBody = joint.connectedBody;

        // remove the two way linking from the other cube, so it's removal status is updated immediately
        Destroy(otherCubeNode.cube.GetComponents<FixedJoint>().First(j => j.connectedBody.gameObject.GetInstanceID() == cube.GetInstanceID()));

        bool found = false;
        for (int i=0; i < otherCubeNode.indicators.Length; i++)
        {
            if (otherCubeNode.indicators[i] == null)
            {
                otherCubeNode.indicators[i] = indicator;
                found = true;
                break;
            }
        }

        if (!found)
        {
            Debug.LogError("Couldn't find free indicator slot in Cube " + otherCubeId);
        }

        cube.GetComponent<Animator>().SetTrigger("Vanish");
        cubeDisappear.Play();
        cubeAmount--;
    }

    private int GetIndicatorNeighbourCount(GameObject indicator)
    {
        Transform t = indicator.transform;

        Vector3[] pos = GetIndicatorWorldPositions(t);

        int amount = 0;
        for(int i=0; i < pos.Length; i++)
        {
            if (CubeExistsAt(pos[i], t.rotation))
                amount++;
        }
        return amount;
    }

    private void RemoveIndicatorsWithTooManyNeighbours()
    {
        for(int i=0; i < nodes.Count; i++)
        {
            Node n = nodes[i];
            for (int j=0; j < n.indicators.Length; j++)
            {
                GameObject ind = n.indicators[j];
                if (!ind)
                    continue;

                if (GetIndicatorNeighbourCount(ind) > 1)
                {
                    n.indicators[j] = null;
                    Destroy(ind);
                }
            }
        }
    }

    public Vector3 GetCenterPoint()
    {
        if (nodes.Count() == 0)
            return startPos.position;
        Vector3 result = Vector3.zero;
        for (int i=0; i < nodes.Count(); i++)
        {
            result += nodes[i].cube.transform.position;
        }
        result /= nodes.Count();
        return result;
    }

    public void AddForce(Vector3 force, ForceMode mode)
    {

        //nodes.Select(n => n.cube.GetComponent<Rigidbody>()).ToList().ForEach(r => r.AddForce(force, mode));
        foreach(Node n in nodes)
        {
            n.cube.GetComponent<Rigidbody>().AddForce(force, mode);
            foreach(GameObject g in n.indicators)
            {
                if (g == null)
                    continue;
                g.GetComponent<Rigidbody>().AddForce(force, mode);
            }
        }
    }
}
