﻿using UnityEngine;
using System.Collections;
using System;

public class Indicator : MonoBehaviour {
    public GameObject cubePrefab;
    private float createdTimestamp;
    const float removalCooldown = 0.05f; // this long has to pass from creation until the indicator can be swapped to cube

    private Animator animator;
    private bool mouseIsOver = false, indicatorActive = false;
    private Vector3 newCubeHalfExtents;
    void Start()
    {
        newCubeHalfExtents = new Vector3(0.25f, 0.25f, 0.25f);
        animator = GetComponent<Animator>();
        createdTimestamp = Time.time;
    }
    void OnMouseEnter()
    {
        mouseIsOver = true;
    }

    void OnMouseExit()
    {
        mouseIsOver = false;
    }

    void OnMouseOver()
    {
        if (!Input.GetMouseButton(0))
            return;

        if (Time.time - createdTimestamp < removalCooldown)
            return;

        if (!FreeSpaceExists())
            return;

        Debug.Log(gameObject.name + " turning to cube");
        GetComponentInParent<Grid>().SwapToCube(gameObject);
    }

    private bool FreeSpaceExists()
    {
        Collider[] collisions = Physics.OverlapBox(transform.position, newCubeHalfExtents, transform.rotation);
        for (int i=0; i < collisions.Length; i++)
        {
            if (!collisions[i].isTrigger)
                return false;
        }
        return true;
    }

    void Update()
    {
        if (mouseIsOver)
        {
            if (indicatorActive)
            {
                if (!FreeSpaceExists())
                    StopIndicating();
                
            }
            else
            {
                if (FreeSpaceExists())
                    StartIndicating();
            }
        }
        else
        {
            if (indicatorActive)
                StopIndicating();
        }
    }

    private void StopIndicating()
    {
        animator.ResetTrigger("FadeIn");
        animator.SetTrigger("FadeOut");
        indicatorActive = false;
    }

    private void StartIndicating()
    {
        animator.ResetTrigger("FadeOut");
        animator.SetTrigger("FadeIn");
        indicatorActive = true;
    }
}
