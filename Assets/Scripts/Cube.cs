﻿using UnityEngine;
using System.Collections;
using System.Linq;

public class Cube : MonoBehaviour {
    private bool removing = false;
    private float createdTimestamp;
    const float removalCooldown = 0.5f; // this long has to pass from creation until the cube can be removed

    void Start()
    {
        createdTimestamp = Time.time;
    }

    private bool CanBeRemoved()
    {
        int connections = GetComponents<FixedJoint>().Where(j => j.connectedBody != null).Count();
        return connections == 1;
    }

    void OnMouseOver()
    {
        if (!Input.GetMouseButton(0)) 
            return;

        if (removing)
            return;

        if (Time.time - createdTimestamp < removalCooldown)
            return;

        if (CanBeRemoved())
        {
            Debug.Log(gameObject.name + " removing");
            GetComponentInParent<Grid>().SwapToIndicator(gameObject);
            removing = true;
        }
            
    }

    // called from animation
    public void DestroyCube()
    {
        Destroy(gameObject);
    }
}
