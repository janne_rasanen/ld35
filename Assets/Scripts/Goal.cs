﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class Goal : MonoBehaviour {
    public GameObject fadeToBlack;

    void OnTriggerEnter(Collider other)
    {
        if (!other.GetComponent<Cube>())
            return;

        fadeToBlack.SetActive(true);

        Invoke("NextLevel", 1f);
    }

    private void NextLevel()
    {
        int nextIndex = SceneManager.GetActiveScene().buildIndex + 1;
        if (nextIndex > 2)
            nextIndex = 0;

        Debug.Log("Loading scene " + nextIndex);
        SceneManager.LoadScene(nextIndex, LoadSceneMode.Single);
    }
}
