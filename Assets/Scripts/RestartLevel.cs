﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class RestartLevel : MonoBehaviour {
    public GameObject fadeToBlack;

    void OnTriggerEnter(Collider other)
    {
        if (!other.GetComponent<Cube>())
            return;

        fadeToBlack.SetActive(true);

        Invoke("Restart", 1f);
    }

    private void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex, LoadSceneMode.Single);
    }
}