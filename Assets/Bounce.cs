﻿using UnityEngine;
using System.Collections;

public class Bounce : MonoBehaviour {

    public AudioSource bounceAudio;

    public AnimationCurve audioVolume;
    public AnimationCurve velocityMultiplier;

    private float coolDownLength = 0.5f;
    private float lastBounce = 0f;
    private Animator animator;

	void Start () {
        animator = GetComponent<Animator>();
	}

    void OnCollisionEnter(Collision collision)
    {
        if (!collision.rigidbody)
            return;


        if (Time.time - lastBounce < coolDownLength)
            return;

        lastBounce = Time.time;
        // animator.SetTrigger("Bounce");

        bounceAudio.volume = audioVolume.Evaluate(Mathf.Abs(collision.relativeVelocity.y));
        bounceAudio.Play();

        float vel = Mathf.Abs(collision.relativeVelocity.y);
        float multiplier = velocityMultiplier.Evaluate(vel);
        
        collision.gameObject.GetComponentInParent<Grid>().AddForce(Vector3.up * vel * multiplier, ForceMode.VelocityChange);
    }
}
